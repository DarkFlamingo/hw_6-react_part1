import Chat from './chat/chat';

const url = 'https://edikdolynskyi.github.io/react_sources/messages.json';

const App = () => {
  return <Chat url={url} />;
};

export default App;
