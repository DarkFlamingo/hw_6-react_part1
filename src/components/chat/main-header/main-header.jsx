import './style.css';
import { Image } from '../../common/common';

const MainHeader = () => {
  return (
    <div className="main-header w-100">
      <div className="container">
        <div className="main-header__wrapper">
          <Image className="main-header__logo" src="/logo.jpg" alt="Logo:(" />
          <span className="main-header__name">Chatalk</span>
        </div>
      </div>
    </div>
  );
};

export default MainHeader;
