import './style.css';
import { Form, Button } from '../../../common/common';
import { useState, useEffect } from 'react';

const MessageInput = ({ addMessage, updateMessage, editableMessage }) => {
  const [text, setText] = useState('');

  const handleSubmit = (event) => {
    if (editableMessage) {
      updateMessage(editableMessage.id, text);
    } else {
      addMessage(text);
    }
    setText('');
    event.preventDefault();
  };

  const textChanged = (data) => {
    setText(data);
  };

  useEffect(() => {
    if (editableMessage) {
      setText(editableMessage.text);
    }
  }, [editableMessage]);

  return (
    <div className="message-input">
      <Form className="form-wrapper" onSubmit={handleSubmit}>
        <Form.Group className="form-group">
          <Form.Control
            className="message-input-text"
            onChange={(ev) => textChanged(ev.target.value)}
            value={text}
          ></Form.Control>
          <Button
            className="message-input-button"
            variant="primary"
            type="submit"
          >
            Send
          </Button>
        </Form.Group>
      </Form>
    </div>
  );
};

export default MessageInput;
