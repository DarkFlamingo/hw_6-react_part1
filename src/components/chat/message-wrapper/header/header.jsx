import './style.css';
import { HEADER_DATE_FORMAT } from '../../../../common/constants/constants';
import moment from 'moment';

const Header = ({ name, lastMessage, quantityOfMessages, quantityOfUsers }) => {
  return (
    <div className="header">
      <div className="header-title">{name}</div>
      <div className="additinal-text">Users: </div>
      <div className="header-users-count">{quantityOfUsers}</div>
      <div className="additinal-text">Messages: </div>
      <div className="header-messages-count">{quantityOfMessages}</div>
      <div className="header-last-message-date">
        {moment(lastMessage.createdAt).format(HEADER_DATE_FORMAT)}
      </div>
    </div>
  );
};

export default Header;
