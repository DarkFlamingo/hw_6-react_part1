import './style.css';
import { Button } from '../../../../common/common';
import { MESSSAGE_DATE_FORMAT } from '../../../../../common/constants/constants';
import moment from 'moment';

const OwnMessage = ({ message, deleteMessage, editMessage }) => {
  const handleEdit = (message) => {
    editMessage(message);
  };

  const handleDelete = (id) => {
    deleteMessage(id);
  };

  return (
    <div className="own-message col-7">
      <p className="message-time">
        {moment(message.createdAt).format(MESSSAGE_DATE_FORMAT)}
      </p>
      <Button variant="secondary" className="message-edit" onClick={() => handleEdit(message)}>
        Edit
      </Button>
      <Button
        variant="danger"
        className="message-delete"
        onClick={() => handleDelete(message.id)}
      >
        Delete
      </Button>
      <p className="message-text">{message.text}</p>
    </div>
  );
};

export default OwnMessage;
