import './style.css';
import moment from 'moment';

const convertDateStringToPrettyString = (dateString) => {
  let now = moment(new Date());
  let end = moment(dateString);
  const difference = now.diff(end, 'days');
  const number = dateString.split(' ');
  number.pop();
  switch (difference) {
    case 0:
      return 'Today';
    case 1:
      return 'Yesterday';
    default:
      return number.join(' ');
  }
};

const MessagesDivider = ({ dateString, prevDateString }) => {
  return (
    <div className="messages-divider col-4">
      {convertDateStringToPrettyString(dateString, prevDateString)}
    </div>
  );
};

export default MessagesDivider;
