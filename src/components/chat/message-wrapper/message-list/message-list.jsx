import React from 'react';
import './style.css';
import Message from './message/message';
import OwnMessage from './own-message/own-message';
import { MAP_DATE_FORMAT } from '../../../../common/constants/constants';
import MessagesDivider from './messages-divider/messages-divider';
import moment from 'moment';

const getMapByDays = (messages) => {
  const mapByDays = new Map();
  messages.forEach((message) => {
    const dateInRightFormat = moment(message.createdAt).format(MAP_DATE_FORMAT);
    if (mapByDays.has(dateInRightFormat)) {
      const array = mapByDays.get(dateInRightFormat);
      array.push(message);
      mapByDays.set(dateInRightFormat, array);
    } else {
      mapByDays.set(dateInRightFormat, [message]);
    }
  });
  return mapByDays;
};

const MessageList = ({ messages, me, deleteMessage, editMessage }) => {
  const messageMap = messages ? getMapByDays(messages) : null;

  return (
    <div className="message-list">
      {Array.from(messageMap).map(([key, value]) => (
        <React.Fragment key={key}>
          <MessagesDivider dateString={key} />
          {value.map((item) =>
            item.userId === me.id ? (
              <OwnMessage
                message={item}
                deleteMessage={deleteMessage}
                editMessage={editMessage}
                key={item.id}
              />
            ) : (
              <Message message={item} key={item.id} />
            )
          )}
        </React.Fragment>
      ))}
    </div>
  );
};

export default MessageList;
