import './style.css';
import MessageList from './message-list/message-list';
import MessageInput from './message-input/message-input';
import Header from './header/header';

const MessageWrapper = ({
  messages,
  me,
  addMessage,
  deleteMessage,
  updateMessage,
  editMessage,
  editableMessage,
}) => {
  const getLastMessage = (messages) => {
    let lastMessage = null;
    messages.forEach((message, index) => {
      if (index === 0 || message.createdAt >= lastMessage.createdAt) {
        lastMessage = message;
      }
    });
    return lastMessage;
  };

  const getQuantityOfUsers = (messages) => {
    let userIdArray = [];
    messages.forEach((message) => {
      if (userIdArray.indexOf(message.userId) === -1) {
        userIdArray.push(message.userId);
      }
    });
    return userIdArray.length;
  };

  return (
    <div className="message-wrapper w-100">
      <div className="container">
        <Header
          name="Binary Chat"
          lastMessage={getLastMessage(messages)}
          quantityOfMessages={messages.length}
          quantityOfUsers={getQuantityOfUsers(messages)}
        />
        <MessageList
          messages={messages}
          me={me}
          deleteMessage={deleteMessage}
          editMessage={editMessage}
        />
        <MessageInput
          addMessage={addMessage}
          updateMessage={updateMessage}
          editableMessage={editableMessage}
        />
      </div>
    </div>
  );
};

export default MessageWrapper;
