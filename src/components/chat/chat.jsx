import './reset.css';
import './style.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import MainHeader from './main-header/main-header';
import MessageWrapper from './message-wrapper/message-wrapper';
import Footer from './footer/footer';
import React, { useState, useEffect } from 'react';
import { Preloader } from '../common/common';
import { v4 as uuidv4 } from 'uuid';
import { USER_DEFAULT_URL } from '../../common/constants/constants';

const userId = uuidv4();

const Chat = ({ url }) => {
  const [messages, setMessages] = useState(null);
  const [editableMessage, setEditableMessage] = useState(null);
  const me = { id: userId, name: 'User' };

  useEffect(() => {
    getMessages(url).then((data) => setMessages(data));
  }, [url]);

  const getMessages = async (url) =>
    fetch(`${url}`).then((res) => {
      return res.json();
    });

  const addMessage = (text) => {
    if (text) {
      setMessages([
        ...messages,
        {
          id: uuidv4(),
          userId: me.id,
          avatar: USER_DEFAULT_URL,
          user: me.name,
          text,
          createdAt: new Date(Date.now()).toISOString(),
          editedAt: '',
        },
      ]);
    }
  };

  const deleteMessage = (id) => {
    setMessages([...messages.filter((message) => message.id !== id)]);
  };

  const updateMessage = (id, text) => {
    if (text) {
      setMessages([
        ...messages.map((message) =>
          message.id !== id
            ? message
            : {
                ...message,
                text: text,
                updatedAt: new Date(Date.now()).toISOString(),
              }
        ),
      ]);
      setEditableMessage(null);
    }
  };

  const editMessage = (messsage) => {
    setEditableMessage(messsage);
  };

  return (
    <div className="chat">
      <MainHeader />
      {messages ? (
        <MessageWrapper
          messages={messages}
          me={me}
          addMessage={addMessage}
          deleteMessage={deleteMessage}
          updateMessage={updateMessage}
          editMessage={editMessage}
          editableMessage={editableMessage}
        />
      ) : (
        <Preloader />
      )}
      <Footer />
    </div>
  );
};

export default Chat;
